"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var minimist = require("minimist");
var path = require("path");
var fs = require("fs");
var yaml = require("js-yaml");
var request = require("request");
function processSettings(next) {
    var options;
    var settingsPath = '/api-code-generator.json';
    var args = minimist(process.argv);
    var projectPath = path.resolve(process.cwd());
    var directoryPath = path.resolve(__dirname);
    if (args.hasOwnProperty('projectPath')) {
        projectPath = args.projectPath;
    }
    if (args.hasOwnProperty('settings')) {
        settingsPath = args.settings;
    }
    console.log('projectPath', projectPath);
    console.log('directoryPath', directoryPath);
    options = JSON.parse(fs.readFileSync(projectPath + '/' + settingsPath, { encoding: 'utf-8' }));
    var defaultOptions = {
        apiSpecPath: 'apispec.yaml',
        apiSpec: null,
        generateMock: true,
        generateGateway: true,
        generateApiTest: true,
        mockOutputFolder: '/mock_server/generated/',
        gatewayOutputFolder: '/src/app/services/',
        apiTestOutputFolder: '/e2e/api-test/generated/',
        projectPath: projectPath,
        directoryPath: directoryPath
    };
    if (options.hasOwnProperty('apiSpecUrl') && options.apiSpecUrl) {
        process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
        request.get(options.apiSpecUrl, function (error, response, body) {
            body = clearApiJsonString(body);
            options.apiSpec = JSON.parse(body);
            next(Object.assign(defaultOptions, options));
        });
    }
    else {
        var apiString = fs.readFileSync(options.apiSpecPath, { encoding: 'utf-8' });
        apiString = clearApiJsonString(apiString);
        options.apiSpec = yaml.load(apiString);
        next(Object.assign(defaultOptions, options));
    }
}
exports.processSettings = processSettings;
function clearApiJsonString(json) {
    return json
        .replace(new RegExp(',string»', 'g'), '')
        .replace(new RegExp('«', 'g'), '_')
        .replace(new RegExp('»', 'g'), '');
}
//# sourceMappingURL=process-settings.js.map