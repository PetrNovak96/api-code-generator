"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var api_base_1 = require("../model/api-base");
// TN means it is type named by property name
// TP means type named by property path - can be same like named by name, so diversed
/**
 * Api Documentation
 */
var GatewayService = /** @class */ (function () {
    function GatewayService(http) {
        this.http = http;
        this.domain = '';
    }
    /**
     * Download fotky občanky, pasu, výpisu.
     */
    GatewayService.prototype.getAccStatementReqStmt = function (parameters) {
        var url = api_base_1.apiBase + "/accStatement/reqStmt";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getAcceptationExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/acceptation/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postAcceptationProcess = function (parameters) {
        var url = api_base_1.apiBase + "/acceptation/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getApprovalExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/approval/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postApprovalProcess = function (parameters) {
        var url = api_base_1.apiBase + "/approval/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getApprovalEmailExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/approvalEmail/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postApprovalEmailProcess = function (parameters) {
        var url = api_base_1.apiBase + "/approvalEmail/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getBasicInfoExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/basicInfo/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postBasicInfoProcess = function (parameters) {
        var url = api_base_1.apiBase + "/basicInfo/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentInsuranceAgreementDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/agreement/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentInsuranceAgreementExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/agreement/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentInsuranceAgreementOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/agreement/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentInsuranceAgreementProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/agreement/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentInsuranceConditionsDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/conditions/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentInsuranceConditionsExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/conditions/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentInsuranceConditionsOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/conditions/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentInsuranceConditionsProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/conditions/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentInsuranceGdprDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/gdpr/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentInsuranceGdprExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/gdpr/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentInsuranceGdprOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/gdpr/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentInsuranceGdprProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/gdpr/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentInsuranceInfolistDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/infolist/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentInsuranceInfolistExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/infolist/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentInsuranceInfolistOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/infolist/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentInsuranceInfolistProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/insurance/infolist/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentLoanAgreementDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/loanAgreement/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentLoanAgreementExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/loanAgreement/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentLoanAgreementOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/loanAgreement/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentLoanAgreementProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/loanAgreement/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentMarketingDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/marketing/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentMarketingExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/marketing/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentMarketingOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/marketing/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentMarketingProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/marketing/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentPrecontractualDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/precontractual/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentPrecontractualExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/precontractual/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentPrecontractualOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/precontractual/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentPrecontractualProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/precontractual/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentPrivacyPolicyDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/privacyPolicy/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentPrivacyPolicyExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/privacyPolicy/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentPrivacyPolicyOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/privacyPolicy/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentPrivacyPolicyProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/privacyPolicy/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentTariffForCitizensDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/tariffForCitizens/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentTariffForCitizensExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/tariffForCitizens/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentTariffForCitizensOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/tariffForCitizens/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentTariffForCitizensProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/tariffForCitizens/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * download
     */
    GatewayService.prototype.getDocumentTermsAndConditionsForConsumerLoansDownload = function (parameters) {
        var url = api_base_1.apiBase + "/document/termsAndConditionsForConsumerLoans/download";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getDocumentTermsAndConditionsForConsumerLoansExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/document/termsAndConditionsForConsumerLoans/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * opened
     */
    GatewayService.prototype.postDocumentTermsAndConditionsForConsumerLoansOpened = function (parameters) {
        var url = api_base_1.apiBase + "/document/termsAndConditionsForConsumerLoans/opened";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postDocumentTermsAndConditionsForConsumerLoansProcess = function (parameters) {
        var url = api_base_1.apiBase + "/document/termsAndConditionsForConsumerLoans/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getEmailAddressExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/emailAddress/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postEmailAddressProcess = function (parameters) {
        var url = api_base_1.apiBase + "/emailAddress/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getEmailCodeExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/emailCode/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postEmailCodeProcess = function (parameters) {
        var url = api_base_1.apiBase + "/emailCode/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getFinanceInfoExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/financeInfo/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postFinanceInfoProcess = function (parameters) {
        var url = api_base_1.apiBase + "/financeInfo/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getFinishExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/finish/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postFinishProcess = function (parameters) {
        var url = api_base_1.apiBase + "/finish/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * currentStep
     */
    GatewayService.prototype.getFlowCurrentStep = function (parameters) {
        var url = api_base_1.apiBase + "/flow/currentStep";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * interrupt
     */
    GatewayService.prototype.postFlowInterrupt = function (parameters) {
        var url = api_base_1.apiBase + "/flow/interrupt";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * invalidation
     */
    GatewayService.prototype.postFlowInvalidation = function (parameters) {
        var url = api_base_1.apiBase + "/flow/invalidation";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * nextStep
     */
    GatewayService.prototype.postFlowNextStep = function (parameters) {
        var url = api_base_1.apiBase + "/flow/nextStep";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var out = {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * create
     */
    GatewayService.prototype.postInitCreate = function (parameters) {
        var url = api_base_1.apiBase + "/init/create";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['item'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * init
     */
    GatewayService.prototype.postInitLoad = function (parameters) {
        var url = api_base_1.apiBase + "/init/load";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * Pouze demonstrativní ukázka vyplnění JSON
     */
    GatewayService.prototype.postInitPocLoan = function (parameters) {
        var url = api_base_1.apiBase + "/init/poc/loan";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getInsuranceExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/insurance/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postInsuranceProcess = function (parameters) {
        var url = api_base_1.apiBase + "/insurance/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getLoanParamsData = function (parameters) {
        var url = api_base_1.apiBase + "/loanParams/data";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getPhoneNumberExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/phoneNumber/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postPhoneNumberProcess = function (parameters) {
        var url = api_base_1.apiBase + "/phoneNumber/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getPhoneNumberCodeExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/phoneNumberCode/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postPhoneNumberCodeProcess = function (parameters) {
        var url = api_base_1.apiBase + "/phoneNumberCode/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * config
     */
    GatewayService.prototype.getPublishedConfig = function (parameters) {
        var url = api_base_1.apiBase + "/published/config";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * enums
     */
    GatewayService.prototype.getPublishedEnums = function (parameters) {
        var url = api_base_1.apiBase + "/published/enums";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getUtmParamsExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/utmParams/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postUtmParamsProcess = function (parameters) {
        var url = api_base_1.apiBase + "/utmParams/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    /**
     * existingData
     */
    GatewayService.prototype.getValidOcrExistingData = function (parameters) {
        var url = api_base_1.apiBase + "/validOcr/existingData";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        return this.http.get(url, queryParameters);
    };
    /**
     * process
     */
    GatewayService.prototype.postValidOcrProcess = function (parameters) {
        var url = api_base_1.apiBase + "/validOcr/process";
        var queryParameters = {};
        if (parameters.$queryParameters) {
            queryParameters = parameters.$queryParameters;
        }
        var p = parameters['input'];
        var out = p ? p : {};
        return this.http.post(url, out, queryParameters);
    };
    GatewayService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.HttpClient !== "undefined" && http_1.HttpClient) === "function" && _a || Object])
    ], GatewayService);
    return GatewayService;
    var _a;
}());
exports.GatewayService = GatewayService;
//# sourceMappingURL=gateway.service.js.map