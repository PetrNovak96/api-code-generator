import * as fs from 'fs';
import { MockGen } from './lib/mock-gen';
import { CodegenOptions } from './lib/types';
import { ProcessSettings, processSettings } from './process-settings';

function generate(settings: ProcessSettings) {
    const options: CodegenOptions = {
        moduleName: 'mock',
        className: 'MockServer',
        swagger: settings.apiSpec,
        imports: [],
        template: {
            class: fs.readFileSync(settings.directoryPath + '/../templates/mock/class.mustache', 'utf-8'),
            method: fs.readFileSync(settings.directoryPath + '/../templates/mock/method.mustache', 'utf-8'),
            type: fs.readFileSync(settings.directoryPath + '/../templates/mock/type.mustache', 'utf-8'),
            default_requests: fs.readFileSync(settings.directoryPath + '/../templates/mock/default-requests.mustache', 'utf-8')
        },
    };

    fs.writeFileSync(settings.projectPath + settings.mockOutputFolder + 'methods.ts', MockGen.generateMockServer(options, 'class'));
    fs.writeFileSync(settings.projectPath + settings.mockOutputFolder + 'requests.ts', MockGen.generateMockServer(options, 'default_requests'));
}

processSettings(generate);
