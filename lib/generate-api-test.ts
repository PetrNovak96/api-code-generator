import * as fs from 'fs';
import { CodeGen } from './lib/codegen';
import { CodegenOptions } from './lib/types';
import { processSettings, ProcessSettings } from './process-settings';

function generate(settings: ProcessSettings)  {
    const options: CodegenOptions = {
        moduleName: 'gateway',
        className: 'GatewayService',
        swagger: settings.apiSpec,
        imports: [],
        template: {
            class: fs.readFileSync(settings.directoryPath + '/../templates/api-test/class.mustache', 'utf-8'),
            method: fs.readFileSync(settings.directoryPath + '/../templates/api-test/method.mustache', 'utf-8'),
            type: fs.readFileSync(settings.directoryPath + '/../templates/shared/type.mustache', 'utf-8'),
            definitions: fs.readFileSync(settings.directoryPath + '/../templates/shared/definitions.mustache', 'utf-8'),
        },
    };
    fs.writeFileSync(settings.projectPath + settings.apiTestOutputFolder + 'gateway.service.ts', CodeGen.getCustomCode(options, 'class'));
}

processSettings(generate);
