import * as minimist from 'minimist';
import * as path from 'path';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import { Swagger } from './lib/types';
import * as request from 'request';

export interface ProcessSettings {
    apiSpecUrl?: string;
    apiSpecPath: string;
    apiSpec: Swagger;
    generateMock: boolean;
    generateGateway: boolean;
    generateApiTest: boolean;
    mockOutputFolder: string;
    gatewayOutputFolder: string;
    apiTestOutputFolder: string;
    projectPath: string;
    directoryPath: string;
}

export function processSettings(next: (out: ProcessSettings) => void) {
    let options: ProcessSettings;
    let settingsPath = '/api-code-generator.json';
    let args = minimist(process.argv);
    let projectPath = path.resolve(process.cwd());
    const directoryPath = path.resolve(__dirname);

    if (args.hasOwnProperty('projectPath')) {
        projectPath = args.projectPath;
    }

    if (args.hasOwnProperty('settings')) {
        settingsPath = args.settings;
    }

    console.log('projectPath', projectPath);
    console.log('directoryPath', directoryPath);

    options = JSON.parse(fs.readFileSync(projectPath + '/' + settingsPath, {encoding: 'utf-8'}));

    const defaultOptions: ProcessSettings = {
        apiSpecPath: 'apispec.yaml',
        apiSpec: null,
        generateMock: true,
        generateGateway: true,
        generateApiTest: true,
        mockOutputFolder: '/mock_server/generated/',
        gatewayOutputFolder: '/src/app/services/',
        apiTestOutputFolder: '/e2e/api-test/generated/',
        projectPath: projectPath,
        directoryPath: directoryPath
    };

    if (options.hasOwnProperty('apiSpecUrl') && options.apiSpecUrl) {
        process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
        request.get(options.apiSpecUrl, (error, response, body) => {
            body = clearApiJsonString(body);
            options.apiSpec = JSON.parse(body);
            next(Object.assign(defaultOptions, options));
        });
    } else {
        let apiString = fs.readFileSync(projectPath + '/' + options.apiSpecPath, {encoding: 'utf-8'});
        apiString = clearApiJsonString(apiString);
        options.apiSpec = yaml.load(apiString);
        next(Object.assign(defaultOptions, options));
    }
}

function clearApiJsonString(json: string): string {
    return json
        .replace(new RegExp(',string»', 'g'), '')
        .replace(new RegExp('«', 'g'), '_')
        .replace(new RegExp('»', 'g'), '');
}
