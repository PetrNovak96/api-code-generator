#!/usr/bin/env node

const path = require('path');
const fs = require('fs');
const {exec} = require('child_process');
const projectPath = path.resolve(process.cwd());

let options = JSON.parse(fs.readFileSync(projectPath + '/api-code-generator.json', {encoding: 'utf-8'}));

process.chdir(path.resolve(__dirname) + '/../dist');

if(options.generateMock){
    exec('node ./generate-mock.js --projectPath=' + projectPath, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log(stdout);
    });
}
if(options.generateGateway){
    exec('node ./generate-gateway.js --projectPath=' + projectPath, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log(stdout);
    });
}
if(options.generateMock){
    exec('node ./generate-api-test.js --projectPath=' + projectPath, (err, stdout, stderr) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log(stdout);
    });
}
